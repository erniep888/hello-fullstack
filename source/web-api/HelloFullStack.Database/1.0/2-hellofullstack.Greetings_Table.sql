USE [hello-fullstack]
GO
/****** Object:  Table [dbo].[Greetings]    Script Date: 7/9/2018 4:54:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Greetings](
	[GreetingId] [int] IDENTITY(1,1) NOT NULL,
	[To] [nvarchar](80) NOT NULL,
	[From] [nvarchar](80) NOT NULL,
	[Message] [nvarchar](250) NOT NULL,
	[CreatedAtUTC] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](80) NOT NULL
 CONSTRAINT [PK_Greetings] PRIMARY KEY CLUSTERED 
(
	[GreetingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
