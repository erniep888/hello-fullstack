using HelloFullStack.Business.Services;
using System;
using Xunit;

namespace HelloFullStack.Test.Business
{   
    /// <summary>
    /// This is currently setup as an integration test since it cannot be tested in isolation without a real connection to the database. 
    /// This needs to be refactored into a true unit test that does not require a database connection.
    /// </summary> 
    public class UserService_LoadFullNames_Should
    {
        private readonly UserService _userService;

        public UserService_LoadFullNames_Should()
        {
            _userService = new UserService();
        }
        
        [Fact]
        public void UserService_LoadFullNames_Contains_Rosie_Thompson()
        {
            var fullNames = _userService.LoadFullNames();
            Assert.Contains<String>("Rosie Thompson", fullNames);
        }
    }
}
