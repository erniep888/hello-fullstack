using HelloFullStack.Domain.Models;
using HelloFullStack.Domain.Contexts;
using System.Linq;
using System;
using Xunit;

namespace HelloFullStack.Test.Domain
{
    /// <summary>
    /// This is currently setup as an integration test since it cannot be tested in isolation without a real connection to the database. 
    /// This needs to be refactored into a true unit test that does not require a database connection.
    /// </summary> 
    public class Domain_CRUDGreetings_Should
    {
        public Domain_CRUDGreetings_Should()
        {
        }

        [Fact]
        public void Domain_CreateGreeting_Should()
        {
            using (var db = new HelloFullStackContext())
            {
                var greeting = new Greeting();
                greeting.CreatedAtUtc = DateTime.UtcNow;
                greeting.CreatedBy = "Domain_CreateGreeting_Should";
                greeting.From = "Test FromUser";
                greeting.Message = "Hello FullStack!";
                greeting.To = "Test ToUser";
                db.Greetings.Add(greeting);
                db.SaveChanges();
            }
        }

        [Fact]
        public void Domain_DeleteGreeting_Should()
        {
            using (var db = new HelloFullStackContext())
            {
                var greetings =
                    from greeting in db.Greetings
                    where greeting.CreatedBy == "Domain_CreateGreeting_Should"
                    select greeting;

                var count = greetings.Count<Greeting>();
                foreach (var greeting in greetings)
                {
                    db.Remove<Greeting>(greeting);
                }

                db.SaveChanges();
            }
        }
    }
}
