﻿using HelloFullStack.Domain.Models;
using HelloFullStack.Domain.ModelBuilders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelloFullStack.Domain.Contexts
{
    public class HelloFullStackContext : DbContext
    {
        public HelloFullStackContext()
        {
        }

        public HelloFullStackContext(DbContextOptions<HelloFullStackContext> options)
            : base(options)
        {

        }

        public virtual DbSet<Greeting> Greetings { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=paschall-lap1;Initial Catalog=hello-fullstack;Persist Security Info=True;User ID=hello-fullstack;Password=hello-fullstack");
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var greetingBuilder = new GreetingBuilder();
            greetingBuilder.BuildModel(modelBuilder);
        }

        
    }
}
