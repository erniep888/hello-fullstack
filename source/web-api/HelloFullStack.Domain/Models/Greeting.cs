﻿using System;

namespace HelloFullStack.Domain.Models
{
    public class Greeting
    {
        public int GreetingId { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string Message { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public string CreatedBy { get; set; }
    }
}
