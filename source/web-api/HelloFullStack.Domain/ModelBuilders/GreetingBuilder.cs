﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using HelloFullStack.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelloFullStack.Domain.ModelBuilders
{
    public class GreetingBuilder
    {
        public ModelBuilder BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Greeting>(entity =>
            {
                entity.HasKey(e => e.GreetingId);

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnName("CreatedAtUTC")   // specific to <PackageReference Include="Microsoft.EntityFrameworkCore.SqlServer" Version="2.1.1" />
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(80);

                entity.Property(e => e.From)
                    .IsRequired()
                    .HasMaxLength(80);

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.To)
                    .IsRequired()
                    .HasMaxLength(80);
            });
            return modelBuilder;
        }
    }
}
