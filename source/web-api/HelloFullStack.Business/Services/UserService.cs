using System;
using System.Collections.Generic;

namespace HelloFullStack.Business.Services
{
    public class UserService
    {
        public List<String> LoadFullNames()
        {
            var fullNames =  new List<string>();

            fullNames.Add("Larry Wotaski");
            fullNames.Add("Rosie Thompson");
            fullNames.Add("Sam Iam");

            return fullNames;
        }
    }
}