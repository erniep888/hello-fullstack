Hello Fullstack API
===================

Definition
----------
The Hello FullStack rest web service application.

Prerequisite
------------
    - .NET C# Web API Core 2
    - SQL Server 2017

Install
-------
    - dotnet restore

Build
-------
    - TBD

Development Run
---------------
    - dotnet run .\HelloFullStack.API\

Unit Test
---------
    - dotnet test .\HelloFullStack.Test\ 

End-to-End Test
---------------
    - TBD

All Tests
---------
    - TBD

Services
--------
TBD

Features
--------
    - TBD

Version
----------------
0.0.1

Language
--------
    - C# Core
    
ChangeLog
---------

- 0.0.1
    - initial creation
