# Angular 6, .NET CORE Web API, SQL Server, Elastic Search Stack #

The initial implementation for the Hello FullStack is as follows:

* Angular 6
* .NET CORE Web API
* SQL Server
* Elastic Search

Obviously, the entire stack is overkill but that is the point.  This implementation is intended to educate all contributors and readers of the positives and negatives of various architectural approaches, coding styles, testing strategies, source documentations, and information searching concepts.