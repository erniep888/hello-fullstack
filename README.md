Hello Fullstack
===============

Definition
----------
A small application to demonstrate Angular, IIS, SQL Server, and .NET C# Web API Core 2.

- [Purpose](PURPOSE.md)
- [Technologies Used](TECHNOLOGIES.md)

Prerequisite
------------
- Angular 6
- .NET C# Web API Core 2
- SQL Server 2017
- [Identity Server Creation](IDENTITYSERVERCREATION.md)

Install
-------
    - TBD

Build
-------
    - TBD

Development Run
---------------
    - TBD

Unit Test
---------
    - TBD

End-to-End Test
---------------
    - TBD

All Tests
---------
    - TBD

Services
--------
TBD

Features
--------
    - TBD

Version
----------------
0.0.1

Language
--------
- TypeScript 2.7
- C# Core
    
ChangeLog
---------

- 0.0.1
    - initial creation
