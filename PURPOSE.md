# What is Hello FullStack? #

## Purpose ##

Hello FullStack is a web-based application intended to optimize architectural approaches when it comes to full-stack designs across different technologies.  For example, imagine you are given an opportunity to create an application multiple times in two or more different languages.  Do you believe you would refine or adjust the design?  Even if you do not change the technology stack, most developers refactor their architectures given more time and knowledge.  Therefore, the primary point of this exercise is to complete the functionality, refactor, and then possibly implement in other languages or technologies in order to achieve a hands-on learning experience in architecture and implementations. 

A secondary reason for this application is to brainstorm and share ideas before committing them to a large applications that actually have significant personal or monetary value.  Anyone that reads this should be free to clone, change, and contribute ideas for new approaches and/or technological solutions.

## Description ##

Hello FullStack is an extension of the classic "Hello World!" application.  In this case, Hello FullStack will allow users to say "Hello" to one another.  Therefore, a user may login, view their notifications (which are "Hello's" from other users), say "Hello" to any other user, search all Conversations, and logout.

The process for implementing the complete Hello FullStack product occurs in phases.  

1. Create the Hello World! user interface that has a line drawn thru the word "World!".
2. Allow users to signup, login, logout, and send a "Hello!" to other users.  
3. Allow any user to search all conversations.

## Technologies ##

[Angular 6, .NET Core Web API, SQL Server, Elastic Search](TECHNOLOGIES.md)

## Architectural Layers ##

* User Interface
* API
     * Controllers
* Business
     * Data Transfer Objects (Dtos)
     * Mappers
     * Logic
* Domain
     * Contexts
     * Filters
     * Models
     * ModelBuilders
* Database

## Deployment Structure ##

* Reverse Proxy on Port 6000 ( [IIS configure](https://blogs.msdn.microsoft.com/friis/2016/08/25/setup-iis-with-url-rewrite-as-a-reverse-proxy-for-real-world-apps/) )
     * Angular UI on Port 5900 ( IIS )
     * Web API on Port 5800 ( IIS )
           * SQL Server 2017
           * Elastic Search

## IdentityServer4 ##

This server enables a centralized login logic for web, mobile, native, and services.  It also supports access control across API's.  IdentityServer has a number of jobs and features - including ([ref](http://docs.identityserver.io/en/release/intro/terminology.html)):

* protect your resources
* authenticate users using a local account store or via an external identity provider
* provide session management and single sign-on
* manage and authenticate clients
* issue identity and access tokens to clients
* validate tokens

[Creation Process](IDENTITYSERVERCREATION.md)

## User Interface ##

Mock-Up

![Capture.PNG](https://bitbucket.org/repo/kM9doxe/images/3953175467-Capture.PNG)